const path = require('path');

module.exports = (app, express) => {
  app.use('/govuk-frontend', express.static(path.resolve(__dirname, '../../node_modules/govuk-frontend/govuk/')));
  app.use('/assets', express.static(path.resolve(__dirname, '../../node_modules/govuk-frontend/govuk/assets/')));
};
