module.exports = (app) => app.get('/test-page/:id', (req, res) => res.render(`test-page-${req.params.id}.njk`));
