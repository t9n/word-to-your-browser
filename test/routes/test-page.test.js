const testPage = require('../../app/routes/test-page');

describe('/test-page/:id', () => {
  test('it renders a test page', () => {
    const res = {
      render: (template) => expect(template).toBe('test-page-2.njk'),
    };
    const app = {
      get: (route, cb) => {
        expect(route).toBe('/test-page/:id');
        return cb({ params: { id: '2' } }, res);
      },
    };
    return testPage(app);
  });
});
