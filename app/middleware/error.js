module.exports = (app) => app.use((error, req, res, next) => {
  if (error) {
    res.status(error.response ? error.response.status : 500).json({
      message: `Oh no!: ${error.message}`,
    });
  } else {
    next();
  }
});
