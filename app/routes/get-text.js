const captureWebsite = require('capture-website');
const axios = require('axios');
const { JSDOM } = require('jsdom');

module.exports = (app) => {
  app.post('/get-text', (req, res, next) => axios.get(req.body.url)
    .then(async (result) => {
      const capture = await captureWebsite.base64(req.body.url, {
        width: 770,
        type: 'jpeg',
        fullPage: true,
        launchOptions: {
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
        },
      }).then((data) => `data:image/jpeg;base64,${data.toString('base64')}`);

      const { window } = new JSDOM(result.data);
      const doc = window.document;

      doc.querySelectorAll('script, style').forEach((element) => {
        element.remove();
      });

      const content = doc.querySelector('html').innerHTML
        .replace(/<!--[\s\S]*?-->/g, '')
        .replace(/<[^>]+>/g, '\n')
        .split('\n')
        .filter((c) => c.match(/^\s*$/) === null)
        .map((c) => c.trim());

      res.json({
        content,
        capture,
      });
    }).catch(next));
};
