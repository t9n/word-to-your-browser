const bodyParser = require('body-parser').json();

module.exports = (app) => app.use(bodyParser);
