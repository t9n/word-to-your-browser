const notFound = require('../../app/routes/404');

describe('/test-page', () => {
  test('it renders the test page', () => {
    const res = {
      status: (code) => ({
        send: (msg) => {
          expect(code).toBe(404);
          expect(msg).toBe('Not found');
        },
      }),
    };
    const app = {
      use: (cb) => cb({}, res),
    };
    return notFound(app);
  });
});
