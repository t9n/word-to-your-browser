const error = require('../../app/middleware/error');

describe('error', () => {
  test('it catches errors in the middleware chain', () => {
    const app = {
      use: (cb) => cb({
        response: {
          status: 401,
        },
        message: 'oops',
      }, {}, {
        status: () => ({
          json: (obj) => {
            expect(obj.message).toBe('Oh no!: oops');
          },
        }),
      }, () => {}),
    };
    return error(app);
  });

  test('it sends a default status', () => {
    const app = {
      use: (cb) => cb({
        message: 'oops',
      }, {}, {
        status: (code) => ({
          json: () => {
            expect(code).toBe(500);
          },
        }),
      }, () => {}),
    };
    return error(app);
  });

  test('it calls next when no error is present', () => {
    const mockNext = jest.fn();
    const app = {
      use: (cb) => {
        cb(null, {}, {}, mockNext);
        expect(mockNext).toHaveBeenCalled();
      },
    };
    return error(app);
  });
});
