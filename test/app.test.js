jest.mock('express', () => {
  const mockedExpress = () => ({
    set: jest.fn(),
    get: jest.fn(),
    post: jest.fn(),
    use: jest.fn(),
    listen: jest.fn((p, cb) => cb()),
  });
  Object.defineProperty(mockedExpress, 'static', { value: jest.fn() });
  return mockedExpress;
});

jest.mock('jsdom', () => jest.fn().mockImplementation(() => {}));
jest.mock('nunjucks', () => ({
  configure: () => {},
}));
jest.mock('http', () => ({
  createServer: () => ({
    listen: () => {},
  }),
}));

describe('app', () => {
  test('it starts', () => {
    console.log = jest.fn();
    require('../app/app'); // eslint-disable-line
    expect(console.log).toHaveBeenCalledWith('Started @ http://localhost:3000');
  });
});
