const path = require('path');
const nunjucks = require('nunjucks');
const axios = require('axios');
const captureWebsite = require('capture-website');
const getText = require('../../app/routes/get-text');

nunjucks.configure([
  path.resolve(__dirname, '../../app/views'),
  path.resolve(__dirname, '../../node_modules/govuk-frontend/govuk/'),
  path.resolve(__dirname, '../../node_modules/govuk-frontend/govuk/components'),
], {
  autoescape: true,
});
jest.mock('axios');
jest.mock('capture-website');
captureWebsite.base64.mockResolvedValue(Buffer.from('ABCD'));

describe('/get-text', () => {
  test('it sets up a post route on /get-text', () => {
    const app = {
      post: (route) => {
        expect(route).toBe('/get-text');
      },
    };
    return getText(app);
  });

  test('it gets the content from test page 1', (done) => {
    axios.get.mockResolvedValue({
      data: nunjucks.render('test-page-1.njk'),
    });
    const req = {
      body: {
        url: 'http://loclahost:3000/test-page/1',
      },
    };
    const res = {
      json: (json) => {
        expect(json).toEqual({
          capture: 'data:image/jpeg;base64,QUJDRA==',
          content: [
            'dragon fire test page',
            'Skip to main content',
            'GOV.UK',
            'Dragon fire test page',
            'Menu',
            'Navigation item 1',
            'Navigation item 2',
            'Navigation item 3',
            'alpha',
            'This is a new service – your',
            'feedback',
            'will help us to improve it.',
            'Trail of',
            'breadcrumbs',
            'Back',
            'Heading',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae consequat odio. Vestibulum est leo, facilisis at blandit dignissim, malesuada a enim. Donec eleifend ut dui pellentesque ultrices.',
            'Caption',
            'Heading with caption',
            'Nested caption',
            'Heading with nested caption',
            'link',
            'bigger link',
            'Numbered',
            'list of',
            'things',
            'links in',
            'a list',
            'button',
            'Can you provide more detail?',
            'Do not include personal or financial information like your National Insurance number or credit card details.',
            'You can enter up to 200 characters',
            'Which types of waste do you transport?',
            'Select all that apply.',
            'Waste from animal carcasses',
            'British',
            'including English, Scottish, Welsh and Northern Irish',
            'Text message',
            'Mobile phone number',
            'What is your date of birth?',
            'For example, 31 3 1980',
            'Day',
            'Month',
            'Year',
            'Help with nationality',
            'We need to know your nationality so we can work out which elections you’re entitled to vote in. If you cannot provide your nationality, you’ll have to send copies of identity documents through the post.',
            'What is your address?',
            'Building and street',
            'line 1 of 2',
            'Building and street line 2 of 2',
            'Town or city',
            'County',
            'Postcode',
            'Upload a file',
            'Services and information',
            'Benefits',
            'Departments and policy',
            'How government works',
            'Support links',
            'Help',
            'Cookies',
            'Contact',
            'Built by the',
            'Government Digital Service',
            'All content is available under the',
            'Open Government Licence v3.0',
            ', except where otherwise stated',
            '© Crown copyright',
          ],
        });
        done();
      },
    };
    const app = {
      post: (_, cb) => cb(req, res, (e) => {
        throw e;
      }),
    };
    return getText(app);
  });

  test('it gets the content from test page 2', (done) => {
    axios.get.mockResolvedValue({
      data: nunjucks.render('test-page-2.njk'),
    });
    const req = {
      body: {
        url: 'http://loclahost:3000/test-page/2',
      },
    };
    const res = {
      json: (json) => {
        expect(json).toEqual({
          capture: 'data:image/jpeg;base64,QUJDRA==',
          content: [
            'dragon fire test page 2',
            'Skip to main content',
            'GOV.UK',
            'Dragon fire test page 2',
            'There is a problem',
            'The date your passport was issued must be in the past',
            'Enter a postcode, like AA1 1AA',
            'It can take up to 8 weeks to register a lasting power of attorney if there are no mistakes in the application.',
            'Application complete',
            'Your reference number',
            'HDJ2123F',
            'Where do you live?',
            'Radio group hint text',
            'England',
            'Radio hint text',
            'Scotland',
            'Wales',
            'Northern Ireland',
            'or',
            'I am a British citizen living abroad',
            'Mobile phone number',
            'Sort by',
            'Recently published',
            'Recently updated',
            'Most views',
            'Most comments',
            'Name',
            'Sarah Philips',
            'Change',
            'name',
            'Date of birth',
            '5 January 1978',
            'Change',
            'date of birth',
            'Contact information',
            '72 Guild Street',
            'London',
            'SE23 6FH',
            'Change',
            'contact information',
            'Contact details',
            '07700 900457',
            'sarah.phillips@example.com',
            'Change',
            'contact details',
            'All content is available under the',
            'Open Government Licence v3.0',
            ', except where otherwise stated',
            '© Crown copyright',
          ],
        });
        done();
      },
    };
    const app = {
      post: (_, cb) => cb(req, res, (e) => {
        throw e;
      }),
    };
    return getText(app);
  });

  test('it gets the content from test page 3', (done) => {
    axios.get.mockResolvedValue({
      data: nunjucks.render('test-page-3.njk'),
    });
    const req = {
      body: {
        url: 'http://loclahost:3000/test-page/3',
      },
    };
    const res = {
      json: (json) => {
        expect(json).toEqual({
          capture: 'data:image/jpeg;base64,QUJDRA==',
          content: [
            'dragon fire test page 3',
            'Skip to main content',
            'GOV.UK',
            'Dragon fire test page 3',
            'Dates and amounts',
            'Date',
            'Amount',
            'First 6 weeks',
            '£109.80 per week',
            'Next 33 weeks',
            '£109.80 per week',
            'Total estimated pay',
            '£4,282.20',
            'Contents',
            'Past day',
            'Past week',
            'Past month',
            'Past year',
            'Past day tab',
            'Past week tab',
            'Past month tab',
            'Past year tab',
            'Event name',
            'The name you’ll use on promotional material.',
            'Error:',
            'Enter an event name',
            'Can you provide more detail?',
            'Do not include personal or financial information, like your National Insurance number or credit card details.',
            'Error:',
            'Enter more detail',
            '!',
            'Warning',
            'You can be fined up to £5,000 if you do not register.',
            'All content is available under the',
            'Open Government Licence v3.0',
            ', except where otherwise stated',
            '© Crown copyright',
          ],
        });
        done();
      },
    };
    const app = {
      post: (_, cb) => cb(req, res, (e) => {
        throw e;
      }),
    };
    return getText(app);
  });
});
