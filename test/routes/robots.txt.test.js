const robots = require('../../app/routes/robots.txt');

describe('/robots.txt', () => {
  test('it renders robots.txt', () => {
    const res = {
      type: (type) => expect(type).toBe('text/plain'),
      send: (text) => expect(text).toBe('User-agent: *\nDisallow: /'),
    };
    const app = {
      get: (route, cb) => {
        expect(route).toBe('/robots.txt');
        return cb({}, res);
      },
    };
    return robots(app);
  });
});
