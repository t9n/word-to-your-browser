FROM buildkite/puppeteer
WORKDIR /opt/word-to-your-browser
COPY . .
RUN npm i --no-save --no-optional && npm dedupe && npm run build && npm prune --production
USER node
CMD [ "npm",  "start" ]
