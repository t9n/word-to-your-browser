const helmet = require('helmet');

module.exports = (app) => app.use(helmet({
  contentSecurityPolicy: {
    directives: {
      defaultSrc: ["'self'"],
      scriptSrc: [
        "'self'",
        "'sha256-+6WnXIl4mbFTCARd8N3COQmT3bJJmo32N8q8ZSQAIcU='",
        "'sha256-HBm1KFUBFPQKfK2MGOX9+xx4nKXEp2/drs8HO3Z/FDQ='",
      ],
    },
  },
}));
