const express = require('express');
const cors = require('cors');

const app = express();

require('./lib/nunjucks')(app);

app.use(cors({
  origin: process.env.CORS || '*',
}));

require('./middleware/helmet')(app);
require('./middleware/body-parser')(app);
require('./middleware/static')(app, express);

require('./routes/get-text')(app);
require('./routes/test-page')(app);
require('./routes/robots.txt')(app);

require('./middleware/error')(app);

require('./routes/404')(app);

app.listen(process.env.PORT || 3000, () => console.log(`Started @ http://localhost:${process.env.PORT || 3000}`));
