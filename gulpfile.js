const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

gulp.task('sass', () => gulp
  .src([
    './node_modules/govuk-frontend/govuk/all.scss',
  ])
  .pipe(concat('all.scss'))
  .pipe(
    sass({
      errLogToConsole: true,
      outputStyle: 'compressed',
      includePaths: ['node_modules/govuk-frontend/govuk'],
    }).on('error', sass.logError),
  )
  .pipe(gulp.dest('./node_modules/govuk-frontend/govuk/')));

gulp.task('default', gulp.series('sass'));
